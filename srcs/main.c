/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 17:20:34 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 17:05:55 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

static int		msh_launch(char **args, char *path)
{
	pid_t	pid;

	pid = fork();
	signal(SIGINT, ft_signal2);
	if (pid == 0)
	{
		if (execve(path, args, g_eenv) == -1)
		{
			ft_printf("msh: Command not found: %s\n", path);
		}
		exit(EXIT_SUCCESS);
	}
	else if (pid < 0)
		ft_printf("msh: errors at forking");
	else if (pid > 0)
		wait(&pid);
	return (1);
}

int				msh_execute(char **args)
{
	char	*parsed_path;

	if (args[0] == NULL)
		return (1);
	parsed_path = parse_path(args[0], -1);
	if (!(check_commands(args)))
	{
		if (parsed_path != NULL)
		{
			msh_launch(args, parsed_path);
			free(parsed_path);
			return (1);
		}
		else
			return (access(args[0], 1) ?
			ft_printf("msh: Command not found: %s\n",
			args[0]) : msh_launch(args, args[0]));
	}
	free(parsed_path);
	return (1);
}

char			*read_line(t_history *his)
{
	long	key;
	char	buf[1024];
	char	string[1024];
	int		index[1024];
	char	*kek;

	g_all = (t_all*)ft_memalloc(sizeof(t_all));
	ft_bzero(string, 1024);
	ft_bzero(index, 1024);
	tgetent(buf, getenv("TERM"));
	while (1)
	{
		key = 0;
		tty(1);
		read(STDIN_FILENO, &key, 8);
		if (key == UP)
			his = key_up(his, string);
		else if (key == DOWN)
			his = key_down(his, string);
		else if (check_key(key, string, index) == 0)
			break ;
	}
	tty(0);
	ft_printf("\n");
	free(g_all);
	kek = ft_strcpy(ft_strnew(ft_strlen(string)), string);
	return (kek);
}

static void		msh_loop(void)
{
	char		**args;
	int			i;
	int			ret;
	char		*line;
	t_history	*his;

	i = -1;
	ret = 0;
	his = init_list(" ");
	while (1)
	{
		ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
		signal(SIGINT, ft_signal);
		line = read_line(his);
		make_list(&his, init_list(line));
		args = ft_strsplit(line, ';');
		free(line);
		ret = msh_comands(args);
		free_mas(args);
		if (ret == -1)
			break ;
	}
}

int				main(int ac, char **av, char **eenv)
{
	char	*s;

	(void)ac;
	(void)av;
	init_eenv(eenv);
	if (check_eenv("SHLVL") != -1)
		change_eenv(check_eenv("SHLVL"), "SHLVL",
		s = ft_itoa(ft_atoi(getenv("SHLVL")) + 1));
	else
		add_eenv("SHLVL", "1");
	free(s);
	ft_alias();
	msh_loop();
	return (0);
}
