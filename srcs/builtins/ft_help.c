/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_help.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 22:19:49 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:56:18 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

int		help(void)
{
	ft_putstr("Msh by ghazrak-\n");
	ft_putstr("Type program names and arguments, and hit enter.\n");
	ft_putstr("The following are built in:\n");
	ft_printf("cd\necho\npwd\nmkdir\nrm\nenv\nsetenv\nunsetenv\nhelp\nexit\n");
	ft_putstr("Use the man command for information on other programs.\n");
	return (1);
}
