/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cd.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 23:46:33 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:15:26 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static void		help_msh_cd(char *oldpwd)
{
	char	*tmp;

	tmp = getenv("HOME");
	chdir(tmp) == 0 ? change_pwd(oldpwd) : 0;
}

static void		msh_cd_tild(char *oldpwd, char *args)
{
	char	*tmp;
	char	*tmp2;
	char	*tmp3;

	tmp = getenv("HOME");
	tmp2 = ft_strsub(args, 1, ft_strlen(args) - 1);
	tmp3 = ft_strjoin(tmp, tmp2);
	chdir(tmp3) == 0 ? change_pwd(oldpwd) : 0;
	free(tmp2);
	free(tmp3);
}

void			change_pwd(char *oldpwd)
{
	char	path[1024];
	int		k;

	getcwd(path, 1024);
	(k = check_eenv("PWD")) == -1 ? add_eenv("PWD", path) :
	change_eenv(k, "PWD", path);
	(k = check_eenv("OLDPWD")) == -1 ? add_eenv("OLDPWD", path) :
	change_eenv(k, "OLDPWD", oldpwd);
}

int				check_errors(char **args)
{
	(!(access(args[1], F_OK) == -1)) ? 0 : CD_ERNOTEXIST(args[1], 1);
	(!(access(args[1], R_OK) == -1)) ? 0 : CD_ERPERM(args[1], 1);
	return (0);
}

int				msh_cd(char **args)
{
	char	oldpwd[1024];
	char	*tmp;

	getcwd(oldpwd, 1024);
	if (args[1] == NULL)
		help_msh_cd(oldpwd);
	else if (chdir(args[1]) == 0)
		change_pwd(oldpwd);
	else if (chdir(args[1]) != 0)
	{
		if (ft_strcmp(args[1], "-") == 0)
		{
			tmp = getenv("OLDPWD");
			chdir(tmp) == 0 ? change_pwd(oldpwd) : 0;
		}
		else if (ft_strncmp(args[1], "~", 1) == 0)
			msh_cd_tild(oldpwd, args[1]);
		else
		{
			if (!(check_errors(args)))
				CD_ERNOTDIR(args[1], 1);
		}
	}
	return (1);
}
