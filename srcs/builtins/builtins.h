/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapka                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 20:33:09 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 20:33:09 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef BUILTINS_H
# define BUILTINS_H

# include "../../includes/minishell.h"





/*
**ft_cd.c
*/
void	change_pwd(char *oldpwd);
int		check_errors(char **args);
int		msh_cd(char **args);
/*
**ft_comands.c
*/
int		msh_exit(void);
int		msh_pwd(char **args);
int		check_commands(char **s);
int		msh_comands(char **commands);
/*
**ft_echo.c
*/
int		msh_echo(char **args);
/*
**ft_eenv.c
*/
void	init_eenv(char **eenv);
int		print_eenv(void);
int		check_eenv(char *name);
int		strlen_geenv(void);
/*
**ft_help.c
*/
int		help(void);
/*
**ft_setenv.c
*/
void	change_eenv(int i, char *name, char *value);
char	**realloc_eenv(int new_size);
void	add_eenv(char *name, char *value);
int		set_env(char **args);
/*
**ft_unsetenv.c
*/
int		unset_envv(char **args);
#endif
