/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/18 21:32:05 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 21:17:25 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void	free_mas(char **mas)
{
	int		i;

	i = -1;
	while (mas[++i])
		free(mas[i]);
	free(mas);
}

void		free_list(t_tab *begin)
{
	t_tab	*tmp;
	t_tab	*begin2;
	t_tab	*curr;

	begin2 = begin;
	if (!begin2->next)
	{
		free(begin2->name);
		free(begin2);
		return ;
	}
	curr = begin2->next;
	while (curr != begin2)
	{
		tmp = curr->next;
		free(curr->name);
		free(curr);
		curr = tmp;
	}
	free(begin2->name);
	free(begin2);
}

void	free_vec(void)
{
	int		i;

	i = -1;
	while (++i < g_vec->total)
	{
		free(((char **)g_vec->data)[i]);
	}
	free(g_vec->data);
	free(g_vec);
}

