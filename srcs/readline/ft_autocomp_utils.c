/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp_utils.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 00:06:25 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:50:45 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

t_tab		*utils(t_tab *begin, int flag, char *name, int type)
{
	if (flag == 0)
		begin = init__first_list(name, type);
	else
		make__list(&begin, name, type);
	return (begin);
}
