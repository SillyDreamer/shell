/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp_list.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/15 15:42:22 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 21:17:33 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

t_tab		*init__first_list(char *name, int type)
{
	t_tab	*tmp;

	tmp = (t_tab*)ft_memalloc(sizeof(t_tab));
	tmp->name = ft_strcpy(ft_strnew(ft_strlen(name) + 1), name);
	tmp->len = ft_strlen(name);
	tmp->begin = 1;
	tmp->select = 1;
	tmp->type = type;
	return (tmp);
}

static t_tab		*init__list(char *name, int type)
{
	t_tab	*tmp;

	tmp = (t_tab*)ft_memalloc(sizeof(t_tab));
	tmp->name = ft_strcpy(ft_strnew(ft_strlen(name) + 1), name);
	tmp->len = ft_strlen(name);
	tmp->type = type;
	return (tmp);
}

void		make__list(t_tab **tab, char *name, int type)
{
	t_tab *tmp;

	tmp = *tab;
	while (tmp->next && tmp->next->begin != 1)
		tmp = tmp->next;
	tmp->next = init__list(name, type);
	tmp->next->prev = tmp;
	tmp->next->next = (*tab);
}

void		next_begin(t_tab **begin, int flag)
{
	t_tab	*tmp;

	tmp = *begin;
	if (!tmp)
		return ;
	while (tmp && tmp->select != 1)
		tmp = tmp->next;
	if (tmp->next)
	{
		tmp->select = 0;
		tmp->next->select = 1;
	}
	while (tmp->prev && tmp->begin != 1)
		tmp = tmp->prev;
	print_list(tmp, flag);
}
