/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_globbing_list.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:49:04 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:53:37 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

void		free_glob_list(t_glob *begin)
{
	t_glob	*tmp;
	t_glob	*tmp2;

	tmp = begin;
	while (tmp)
	{
		tmp2 = tmp->next;
		free(tmp->name);
		free(tmp);
		tmp = tmp2;
	}
}

t_glob		*create_glob(char *name)
{
	t_glob	*glob;

	glob = (t_glob*)ft_memalloc(sizeof(t_glob));
	glob->len = ft_strlen(name);
	glob->name = ft_strcpy(ft_strnew(glob->len + 1), name);
	return (glob);
}

t_glob		*init_glob(char *name, t_glob *glob)
{
	while (glob->next)
		glob = glob->next;
	glob->next = create_glob(name);
	return (glob);
}
