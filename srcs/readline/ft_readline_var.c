/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readline_var.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:17:36 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:37:48 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static inline void		delete_var(char *string, char *tmp)
{
	char	*tmp3;
	int		len;

	len = ft_strlen(tmp) + 1;
	while (len--)
		if (g_all->i > 0)
		{
			g_all->j--;
			tmp3 = ft_strsub(string, g_all->j + 1,
			ft_strlen(string) - (g_all->j + 1));
			string = ft_strncpy(string, string, g_all->j);
			string[g_all->j] = '\0';
			string = ft_strcat(string, tmp3);
			g_all->i--;
			free(tmp3);
		}
}

static inline void		add_var(char *string, char *tmp)
{
	char	*tmp2;

	if (g_all->i > 0)
	{
		tmp2 = ft_strsub(string, g_all->j, ft_strlen(string) - g_all->j);
		string = ft_strncpy(string, string, g_all->j);
		*(string + g_all->j) = '\0';
		string = ft_strcat(string, getenv(tmp));
		string = ft_strcat(string, tmp2);
		g_all->i += ft_strlen(getenv(tmp)) - ft_strlen(tmp) + 1;
		g_all->h = g_all->i;
		free(tmp2);
		g_all->j = g_all->i;
	}
	else if (g_all->i == 0)
	{
		ft_strcpy(string, getenv(tmp));
		g_all->j = ft_strlen(getenv(tmp));
		g_all->i = g_all->j;
	}
}

void					check_var(char *string)
{
	int		i;
	char	*tmp;
	int		j;
	char	*tmp2;

	i = 0;
	j = 0;
	tmp = ft_memalloc(sizeof(char) * 1000);
	while (ft_strchr(string, '$'))
	{
		while (string[i] && string[i] != '$')
			i++;
		if (string[i])
		{
			i++;
			while (string[i] && (string[i] != ' ' && string[i] != '\t'))
				tmp[j++] = string[i++];
			g_all->j = i;
			delete_var(string, tmp);
			add_var(string, tmp);
		}
		i = 0;
		j = 0;
	}
	free(tmp);
}
