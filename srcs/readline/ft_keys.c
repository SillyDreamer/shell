/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keys.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/01 05:33:32 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 18:04:24 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static void		key_tab(char *string)
{
	if (g_all->i > 0)
	{
		ft_putstr_fd(tgetstr("vi", NULL), 1);
		if (string[g_all->i - 1] == ' ' || string[g_all->i - 1] == '/')
		{
			if (ft_strstr(string, "cd"))
				complet_cd(string);
			else
				complet_args(string);
		}
		else if (ft_strchr(string, '*') || ft_strchr(string, '?') ||
		(ft_strchr(string, '[') && ft_strchr(string, ']')))
			globing(string);
		else if (string[g_all->i - 1] == '$')
			complet_dollar(string);
		else if (ft_strchr(string, ' '))
			complet_arguments(string);
		else if (!check_path(string, -1))
			;
		else
		{
			ft_putstr("\n");
			complet_command(string, -1, 0);
		}
		ft_putstr_fd(tgetstr("ve", NULL), 1);
	}
	else
	{
		string[g_all->i] = '\t';
		g_all->i++;
		g_all->j++;
		ft_putstr("\t");
	}
}

static int			key_enter(char *string)
{
	if (g_all->quotes == quotes)
			QUOTES
	else if (g_all->quotes == bquotes)
		BQUOTES
	else if (g_all->quotes == dquotes)
		DQUOTES
	else if (string[g_all->i - 1] == '\\')
	{
		string[g_all->i - 1] = '\0';
		g_all->i--;
		g_all->j--;
		ft_putstr("\n>  ");
	}
	else
	{
		check_var(string);
		check_alias(string);
		return (0);
	}
	return (1);
}

int			check_key(long key, char *string, int *index)
{
	if (key == 34 || key == 39 || key == 96)
		check_quotes(key, index);
	if (key == ENTER)
	{
		if (key_enter(string) == 0)
			return (0);
	}
	else if (key == LEFT || key == RIGHT)
		key_right_left(key);
	else if (key == TAB)
		key_tab(string);
	else if (key == BACKSPASE)
		key_backspace(string);
	else if (key == CTRL_LEFT || key == CTRL_RIGHT)
		key_ctrl_left(key, string);
	else if (key == LEFT_LEFT || key == RIGHT_RIGHT)
		left_right(key);
	else if (key == CTRL_E)
		key_shift_e(string);
	else if (key == CTRL_T)
		key_shift_r(string);
	else
		not_keys(key, string);
	return (1);
} 
