/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp_print_list.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:42:21 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:51:04 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static int			display(t_tab *begin)
{
	t_win	win;
	t_tab	*tab;
	int		size;

	tab = begin;
	size = 0;
	while (tab->next && tab->next->begin != 1)
	{
		size++;
		tab = tab->next;
	}
	return (size);
}

static int			max_len(t_tab *tab)
{
	int tmp;

	tmp = 0;
	if (tab->next)
	{
		while (tab->next && tab->next->begin != 1)
		{
			if (tmp < tab->len)
				tmp = tab->len;
			tab = tab->next;
		}
		return (tmp);
	}
	else
		return (0);
}

void		print_list(t_tab *tab, int flag)
{
	t_win	win;
	int		row_num;
	int		count = 0;

	ioctl(STDOUT_FILENO, TIOCGWINSZ, &win);
	int n = max_len(tab) + 2;
	int k = display(tab);
	win.ws_col = win.ws_col > 80 ? win.ws_col - n : win.ws_col;
	row_num = n * k / win.ws_col;
	row_num = n * k % win.ws_col != 0 ? row_num + 1 : row_num + 0;
	int print_num = row_num;
	count =0;
	int i;
	while (row_num--)
	{
		i = count++;
		while (i < k + 1)
		{
			if (tab->select == 1)
			{
				ft_putstr_fd(tgetstr("so", NULL), 1);
				print_type(tab->type, tab->name, flag, n - 2);
				ft_putstr_fd(tgetstr("se", NULL), 1);
				ft_putstr("  ");
			}
			else
			{
				print_type(tab->type, tab->name, flag, n - 2);
				ft_printf("  ");
			}
			tab = tab->next;
			i += print_num;
		}
		write(1, "\n", 1);
	}
	if (!tab->next)
	{
		ft_putstr_fd(tgetstr("so", NULL), 1);
		ft_putstr(tab->name);
		ft_putstr_fd(tgetstr("se", NULL), 1);
	}
}
