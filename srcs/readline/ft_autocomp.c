/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 05:21:24 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:52:18 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

void			complet_command(char *string, int i, int flag)
{
	char		**path;
	DIR			*fd;
	t_dirent	*ret;
	char		*s;
	t_tab		*begin;

	if (!(s = getenv("PATH")))
		return ;
	path = ft_strsplit(s, ':');
	while ((path[++i]))
	{
		fd = opendir(path[i]);
		while ((ret = readdir(fd)))
		{
			if (ft_strncmp(ret->d_name, string, ft_strlen(string)) == 0)
			{
				begin = utils(begin, flag, ret->d_name, ret->d_type);
				flag = 1;
			}
		}
		fd ? closedir(fd) : 0;
	}
	path[0] ? free_mas(path) : 0;
	print(begin, 0, string);
	//free_list(begin);
}

void			complet_args(char *string)
{
	DIR			*fd;
	t_dirent	*ret;
	t_tab		*begin;
	int			flag;
	char		**tmp;

	flag = 0;
	tmp = ft_split_whitespaces(string);
	fd = tmp[1] ? opendir(ft_strjoin("./", tmp[1])) : opendir(".");
	while ((ret = readdir(fd)) > 0)
	{
		if (!(ret->d_name[0] == '.'))
		{
			begin = utils(begin, flag, ret->d_name, ret->d_type);
			flag = 1;
		}
	}
	closedir(fd);
	flag == 1 ? ft_putstr("\n") : 0;
	flag == 1 ? print2(begin, 1, string, 0) : 0;
	flag == 1 ? free_list(begin) : 0;
	free_mas(tmp);
}

void			complet_dollar(char *string)
{
	t_tab	*begin;
	int		i;
	int		j;
	char	*s;
	int		flag;

	flag = 0;
	i = 0;
	j = strlen_geenv();
	while (i < j)
	{
		s = ft_strsub(g_eenv[i], 0, ft_strlen(g_eenv[i]) -
		ft_strlen(ft_strchr(g_eenv[i], '=')));
		begin = utils(begin, flag, s, 8);
		flag = 1;
		free(s);
		i++;
	}
	flag == 1 ? ft_putstr("\n") : 0;
	flag == 1 ? print2(begin, 1, string, 0) : 0;
	flag == 1 ? free_list(begin) : 0;
}

void			complet_cd(char *string)
{
	DIR			*fd;
	t_dirent	*ret;
	t_tab		*begin;
	int			flag;
	char		**tmp;

	tmp = ft_split_whitespaces(string);
	if (tmp[1])
	{
		if (!(fd = opendir(ft_strjoin("./", tmp[1]))))
			return ;
	}
	else
		fd = opendir(".");
	flag = 0;
	while ((ret = readdir(fd)) > 0)
		if (!(ret->d_name[0] == '.') && ret->d_type == 4)
		{
			begin = utils(begin, flag, ret->d_name, ret->d_type);
			flag = 1;
		}
	closedir(fd);
	flag == 1 ? ft_putstr("\n") : 0;
	flag == 1 ? print2(begin, 1, string, 0) : 0;
	flag == 1 ? free_list(begin) : 0;
}

void			complet_arguments(char *string)
{
	DIR			*fd;
	t_dirent	*ret;
	t_tab		*begin;
	int			flag;
	char		**tmp;
	int			i = 0;

	flag = 0;
	tmp = ft_split_whitespaces(string);
	while (tmp[i])
		i++;
	fd = opendir(".");
	while ((ret = readdir(fd)) > 0)
		if (!(ret->d_name[0] == '.') &&
		!(ft_strncmp_registr(ret->d_name, tmp[i - 1],
		ft_strlen(tmp[i - 1]))))
		{
			begin = utils(begin, flag, ret->d_name, ret->d_type);
			flag = 1;
		}
	closedir(fd);
	flag == 1 ? ft_putstr("\n") : 0;
	flag == 1 ? print2(begin, 1, string, 1) : 0;
	flag == 1 ? free_list(begin) : 0;
	free_mas(tmp);
}
