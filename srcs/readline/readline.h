/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapka                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 20:30:42 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 20:30:42 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef READLINE_H
# define READLINE_H

# include "../../includes/minishell.h"





/*
**ft_autocomp.c
*/
void		complet_command(char *string, int i, int flag);
void		complet_args(char *string);
void		complet_dollar(char *string);
void		complet_cd(char *string);
void		complet_arguments(char *string);
/*
**ft_autocomp_list.c
*/
t_tab		*init__first_list(char *name, int type);
void		make__list(t_tab **tab, char *name, int type);
void		next_begin(t_tab **begin, int flag);
void		free_list(t_tab *begin);
/*
**ft_autocomp_print.c
*/
void		print(t_tab *begin, int flag, char *string);
void		print2(t_tab *begin, int flag, char *string, int flag2);
void		print_type(int type, char *name, int flag, int max);
/*
**ft_autocomp_print_list.c
*/
void		print_list(t_tab *tab, int flag);
/*
**ft_autocomp_termcap.c
*/
int			key_tab_tab2(int key, t_tab *begin, int size, char *string);
int			key_tab_tab(int key, t_tab *begin, int size);
void		key_tab_enter(t_tab *begin, char *string);
void		key_tab_enter2(t_tab *begin, char *string);
void		move(int size);
/*
**ft_autocomp_utils.c
*/
t_tab		*utils(t_tab *begin, int flag, char *name, int type);
/*
**ft_brackets_quotes.c
*/
void		check_quotes(long key, int *index_quotes);
/*
**ft_edit_line.c
*/
void		key_backspace(char *string);
void		not_keys(long key, char *string);
void		key_shift_e(char *string);
void		key_shift_r(char *string);
/*
**ft_globbing.c
*/
void		globing(char *string);
/*
**ft_globbing_list.c
*/
void		free_glob_list(t_glob *begin);
t_glob		*create_glob(char *name);
t_glob		*init_glob(char *name, t_glob *glob);
/*
**ft_history.c
*/
t_history	*init_list(char *line);
t_history	*make_list(t_history **list, t_history *elem);
t_history	*key_down(t_history *his, char *string);
t_history	*key_up(t_history *his, char *string);
/*
**ft_keys.c
*/
int			check_key(t_history *his, long key, char *string, int *index);
/*
**ft_keys_move.c
*/
void		key_right_left(long key);
void		key_ctrl_left(long key, char *string);
void		left_right(long key);
/*
**ft_readline_var.c
*/
void		check_var(char *string);
#endif
