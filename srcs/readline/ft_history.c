/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_history.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/28 22:07:49 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 21:25:59 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

t_history	*init_list(char *line)
{
	t_history	*tmp;

	tmp = (t_history*)malloc(sizeof(t_history));
	tmp->next = NULL;
	tmp->prev = NULL;
	tmp->line = ft_strcpy(ft_strnew(ft_strlen(line) + 1), line);
	return (tmp);
}

t_history	*make_list(t_history **list, t_history *elem)
{
	t_history *node;
	t_history *tmp;

	if (list)
	{
		node = *list;
		*list = elem;
		if (node)
		{
			(*list)->next = node;
			node->prev = *(list);
		}
	}
	return (node);
}

t_history	*key_down(t_history *his, char *string)
{
	if (his->prev)
	{
		while (g_all->i != 0)
		{
			ft_putstr_fd(tgetstr("le", NULL), 1);
			ft_putstr_fd(tgetstr("dc", NULL), 1);
			g_all->i--;
		}
		g_all->i = ft_strlen(his->line) - 1;
		ft_printf("%s", his->line);
		string = ft_strcpy(string, his->line);
		his = his->prev;
		g_all->j = g_all->i;
	}
	return (his);
}

t_history	*key_up(t_history *his, char *string)
{
	if (his->next)
	{
		while (g_all->i > 0)
		{
			ft_putstr_fd(tgetstr("le", NULL), 1);
			ft_putstr_fd(tgetstr("dc", NULL), 1);
			g_all->i--;
		}
		g_all->i = ft_strlen(his->line);
		ft_printf("%s", his->line);
		string = ft_strcpy(string, his->line);
		his = his->next;
		g_all->j = g_all->i;
	}
	return (his);
}
