/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_edit_line.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:24:18 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:34:33 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

void		key_backspace(char *string)
{
	char	*tmp;

	if (g_all->i > 0)
	{
		g_all->j--;
		tmp = ft_strsub(string, g_all->j + 1,
		ft_strlen(string) - (g_all->j + 1));
		string = ft_strncpy(string, string, g_all->j);
		string[g_all->j] = '\0';
		string = ft_strcat(string, tmp);
		ft_putstr_fd(tgetstr("le", NULL), 1);
		ft_putstr_fd(tgetstr("dc", NULL), 1);
		g_all->i--;
		free(tmp);
	}
}

void		not_keys(long key, char *string)
{
	char	*tmp;

	if (g_all->i == g_all->j)
	{
		ft_printf("%c", key);
		string[g_all->i++] = key;
		g_all->j++;
	}
	else
	{
		tmp = ft_strsub(string, g_all->j, ft_strlen(string) - g_all->j);
		*(string + g_all->j) = key;
		*(string + g_all->i + 1) = '\0';
		*(string + g_all->j + 1) = '\0';
		string = ft_strcat(string, tmp);
		write(1, &string[g_all->j], 1);
		g_all->h = ++g_all->i;
		ft_putstr_fd(tgetstr("se", NULL), 0);
		ft_putstr(tmp);
		while (--g_all->h > g_all->j)
			ft_printf("\033[%dD", (1));
		free(tmp);
		g_all->j++;
	}
}

void		key_shift_e(char *string)
{
	ft_bzero(g_copy, 1024);
	ft_strncpy(g_copy, string, g_all->j);
}

void		key_shift_r(char *string)
{
	char *tmp;

	if (g_all->i > 0)
	{
		tmp = ft_strsub(string, g_all->j, ft_strlen(string) - g_all->j);
		string = ft_strncpy(string, string, g_all->j);
		*(string + g_all->j) = '\0';
		ft_printf("%s", g_copy);
		string = ft_strcat(string, g_copy);
		string = ft_strcat(string, tmp);
		g_all->i += ft_strlen(g_copy);
		g_all->h = g_all->i;
		ft_putstr(tmp);
		free(tmp);
		g_all->j = g_all->i;
	}
	else if (g_all->i == 0)
	{
		ft_strcpy(string, g_copy);
		g_all->j = ft_strlen(g_copy);
		g_all->i = g_all->j;
		ft_printf("%s", string);
	}
}
