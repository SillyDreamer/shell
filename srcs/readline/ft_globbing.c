/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_globbing.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/19 20:58:18 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:53:40 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static int			match(char *s1, char *s2)
{
	if (*s1 != '\0' && *s2 == '*')
		return (match(s1 + 1, s2) || match(s1, s2 + 1));
	else if (*s1 == '\0' && *s2 == '*')
		return (match(s1, s2 + 1));
	else if ((*s1 == *s2 || *s2 == '?') && *s1 != '\0' && *s2 != '\0')
		return (match(s1 + 1, s2 + 1));
	else if (*s1 == *s2 && *s1 == '\0' && *s2 == '\0')
		return (1);
	return (0);
}

static void		search(char *str, char *string)
{
	DIR			*fd;
	t_dirent	*ret;
	t_glob		*glob;
	t_glob		*begin;
	int			flag;

	flag = 0;
	fd = opendir(".");
	while ((ret = readdir(fd)) > 0)
		if (match(ret->d_name, str))
		{
			if (flag == 0)
			{
				begin = create_glob(ret->d_name);
				flag = 1;
			}
			else
				glob = init_glob(ret->d_name, begin);
		}
	closedir(fd);
	if (flag == 1)
	{
		char	*tmp2;
		char l = ft_strlen(str);
		t_glob *tmp = begin;
		while (l-- > 0)
			key_backspace(string);
		while (begin)
		{
			ft_printf(" %s", begin->name);
			ft_strcat(string, " ");
			ft_strcat(string, begin->name);
			begin = begin->next;
		}
		free_glob_list(tmp);
	}
}

void		globing(char *string)
{
	char	**mas;
	char	str[1024];
	int		i;
	int		j;

	j = 0;
	i = 0;
	mas = ft_split_whitespaces(string);
	while (mas[i])
		i++;
	i--;
	while (i >= 0)
	{
		if (ft_strchr(mas[i], '*') || ft_strchr(mas[i], '?') ||
		(ft_strchr(string, '[') && ft_strchr(string, ']')))
		{
			ft_strcpy(str, mas[i]);
			break ;
		}
		i--;
	}
	i = 0;
	search(str, string);
	free_mas(mas);
}
