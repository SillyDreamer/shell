/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp_print.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 22:23:20 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:50:58 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static int			size_print_in_tty_autocomp(t_tab **begin)
{
	t_win	win;
	t_tab	*tab;
	int		col;
	int		n;
	int		k;

	tab = *begin;
	n = 0;
	k = 0;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &win);
	while (tab->next && tab->next->begin != 1)
	{
		if (n < tab->len)
			n = tab->len;
		tab = tab->next;
		k++;
	}
	n = n + 2;
	win.ws_col = win.ws_col > 80 ? win.ws_col - n : win.ws_col;
	col = n * k / win.ws_col;
	col = n * k % win.ws_col != 0 ? col + 1 : col + 0;
	return (col);
}

void		print(t_tab *begin, int flag, char *string)
{
	long	key;
	int		size;

	key = 0;
	print_list(begin, flag);
	read(STDIN_FILENO, &key, 8);
	size = size_print_in_tty_autocomp(&begin);
	if (key == TAB)
		key = key_tab_tab(key, begin, size);
	move(size);
	if (key == ENTER)
		key_tab_enter(begin, string);
	else
	{
		ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
		ft_putstr(string);
	}
}

void		print2(t_tab *begin, int flag, char *string, int flag2)
{
	long	key;
	int		size;

	key = 0;
	if (flag2 == 1)
		while (string[g_all->i - 1] != ' ')
			key_backspace(string);
	print_list(begin, flag);
	read(STDIN_FILENO, &key, 8);
	size = size_print_in_tty_autocomp(&begin);
	if (key == TAB)
		key = key_tab_tab2(key, begin, size, string);
	move(size);
	if (key == ENTER)
		key_tab_enter2(begin, string);
	else
	{
		ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
		ft_putstr(string);
	}
}

void		print_type(int type, char *name, int flag, int max)
{
	if (flag == 1)
	{
		type == 4 ? ft_printf(BLU "%-*s\x1B[0m", max, name) : 0;
		type == 10 ? ft_printf(MAG "%s" NRM, name) : 0;
		type == 2 ? ft_printf(YEL "%s" NRM, name) : 0;
		type == 6 ? ft_printf(GRN "%s" NRM, name) : 0;
		if (type == 8 && (access(name, 1)))
			ft_printf("%-*s" NRM, max, name);
		else if (type == 8)
			ft_printf(RED "%-*s\x1B[0m", max, name);
	}
	else
	{
		ft_printf("%-*s", max, name);
	}
}