/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_brackets_quotes.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:22:32 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:33:37 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

void		check_quotes(long key, int *index_quotes)
{
	if (key == 39)
	{
		if (g_all->quotes == off)
			g_all->quotes = quotes;
		else if (g_all->quotes == quotes)
			g_all->quotes = off;
	}
	else if (key == 34)
	{
		if (g_all->quotes == off)
			g_all->quotes = dquotes;
		else if (g_all->quotes == dquotes)
			g_all->quotes = off;
	}
	else if (key == 96)
	{
		if (g_all->quotes == off)
			g_all->quotes = bquotes;
		else if (g_all->quotes == bquotes)
			g_all->quotes = off;
	}
}
