/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_autocomp_termcap.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 00:01:14 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:49:17 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

int			key_tab_tab2(int key, t_tab *begin, int size, char *string)
{
	int		col;

	while (key == TAB)
	{
		col = 0;
		while (col++ < size)
		{
			ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		}
		if (size == 0)
		{
			ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		}
		while (begin && begin->select != 1)
			begin = begin->next;
		ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
		ft_putstr(string);
		ft_putstr(begin->next ? begin->next->name : begin->name);
		ft_putstr_fd(tgoto(tgetstr("do", NULL), 0, 0), 1);
		next_begin(&begin, 1);
		key = 0;
		read(STDIN_FILENO, &key, 8);
	}
	return (key);
}

int			key_tab_tab(int key, t_tab *begin, int size)
{
	int		col;

	while (key == TAB)
	{
		col = 0;
		while (col++ < size)
		{
			ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		}
		if (size == 0)
		{
			ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
			ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		}
		while (begin && begin->select != 1)
			begin = begin->next;
		ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
		ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
		ft_putstr(begin->next ? begin->next->name : begin->name);
		ft_putstr_fd(tgoto(tgetstr("do", NULL), 0, 0), 1);
		next_begin(&begin, 0);
		key = 0;
		read(STDIN_FILENO, &key, 8);
	}
	return (key);
}

void		key_tab_enter(t_tab *begin, char *string)
{
	while (begin && begin->select != 1)
		begin = begin->next;
	string = ft_strcpy(string, begin->name);
	g_all->i = ft_strlen(string);
	g_all->j = g_all->i;
	*(string + g_all->i) = '\0';
	ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
	ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
	ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
	ft_putstr(string);
}

void		key_tab_enter2(t_tab *begin, char *string)
{
	while (begin && begin->select != 1)
		begin = begin->next;
	string = ft_strcat(string, begin->name);
	g_all->i = ft_strlen(string);
	g_all->j = g_all->i;
	*(string + g_all->i) = '\0';
	ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
	ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
	ft_putstr("\x1B[32m(づ｡◕‿‿◕｡)づ:  \x1B[0m");
	ft_putstr(string);
}

void		move(int size)
{
	int		col;

	col = 0;
	while (col++ < size)
	{
		ft_putstr_fd(tgoto(tgetstr("up", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
	}
	if (size == 0)
	{
		ft_putstr_fd(tgoto(tgetstr("ch", NULL), 0, 0), 1);
		ft_putstr_fd(tgoto(tgetstr("cd", NULL), 0, 0), 1);
	}
}
