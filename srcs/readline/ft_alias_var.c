/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alias_var.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/28 17:20:55 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 20:28:43 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

static void			change_string(char *string, char *content)
{
	char	*tmp;
	int		i;

	i = 0;
	if (g_all->i > 0)
	{
		ft_bzero(string, 1024);
		g_all->i = 0;
	}
	while (content && content[i] != '=')
		i++;
	i++;
	string = ft_strcpy(string, &content[i]);
}

void				check_alias(char *string)
{
	int		i;
	int		j;
	char	tmp[1024];
	char	tmp2[1024];

	i = -1;
	j = -1;
	ft_bzero(tmp, 1024);
	while (string[++j] && (string[j] != ' ' || string[j] != '\t'))
		tmp[j] = string[j];
	while (++i < g_vec->total)
	{
		ft_bzero(tmp2, 1024);
		j = -1;
		while (((char **)g_vec->data)[i][++j] &&
		((char **)g_vec->data)[i][j] != '=')
			tmp2[j] = ((char **)g_vec->data)[i][j];
		if (ft_strcmp(tmp, tmp2) == 0)
		{
			change_string(string, ((char **)g_vec->data)[i]);
			break ;
		}
	}
}
