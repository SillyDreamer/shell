/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_keys_move.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/24 23:07:40 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:37:09 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

void		key_right_left(long key)
{
	if (key == LEFT)
	{
		if (g_all->j > 0)
		{
			ft_printf("\033[%dD", (1));
			g_all->j--;
		}
	}
	else if (key == RIGHT)
	{
		if (g_all->j < g_all->i)
		{
			ft_printf("\033[%dC", (1));
			g_all->j++;
		}
	}
}

void		key_ctrl_left(long key, char *string)
{
	if (key == CTRL_LEFT)
	{
		while (string[g_all->j] != ' ' && g_all->j > 0)
			key_right_left(LEFT);
		while (string[g_all->j] == ' ' && g_all->j > 0)
			key_right_left(LEFT);
	}
	else if (key == CTRL_RIGHT)
	{
		while (string[g_all->j] != ' ' && g_all->j < g_all->i)
			key_right_left(RIGHT);
		while (string[g_all->j] == ' ' && g_all->j < g_all->i)
			key_right_left(RIGHT);
	}
}

void		left_right(long key)
{
	if (key == LEFT_LEFT)
		while (g_all->j > 0)
			key_right_left(LEFT);
	else if (key == RIGHT_RIGHT)
		while (g_all->j < g_all->i)
			key_right_left(RIGHT);
}
