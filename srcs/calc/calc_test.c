#include <stdio.h>
#include <unistd.h>
#include "rpn_handler.h"
#include "stackft/ft_stack.h"
#include "stackft/rpn_create.h"

int 	main(int ac, char **av)
{
	if (ac == 2)
	{
		printf("%lld\n", rpn_handler(rpn_create(av[1])));
	}
	else
	{
		write(2, "./bin [exp]\n", 12);
		exit(127);
	}
	exit(0);
}