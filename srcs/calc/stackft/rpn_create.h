#ifndef RPN_CREATE_H
# define RPN_CREATE_H

# define IS_DIGIT		1
# define IS_SIGN		2
# define IS_WS			3
# define IS_UNHAND		4
# define IS_OPBR		5
# define IS_CLBR		6

# include "ft_stack.h"

t_stack		*rpn_create(const char *expr);

#endif //INC_21SH_RPN_CREATE_H
