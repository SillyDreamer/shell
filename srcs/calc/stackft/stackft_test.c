#include "ft_stack.h"

int 		main(int ac, char *av[])
{
	t_stack *one = t_stack_create(5, 1);
	t_stack_push(one, 1);
	t_stack_push(one, 2);
	t_stack_push(one, 7);
	t_stack_push(one, 5);
	t_stack_push(one, 3);
	t_stack_push(one, 4);
	t_stack_print(one);
	printf("popping: %d\n", t_stack_pop(one));
	t_stack_print(one);
	t_stack_destroy(one);
	exit(0);
}