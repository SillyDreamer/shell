#include "ft_stack.h"
#include "rpn_create.h"

static int 	char_type(char c)
{
	if ('0' <= c && c <= '9')
		return (IS_DIGIT);
	else if (c == ' ' || c == '\t' || c == '\n'
			|| c == '\v' || c == '\f' || c == '\r')
		return (IS_WS);
	else if (c == '(')
		return (IS_OPBR);
	else if (c == ')')
		return (IS_CLBR);
	else if (c == '+' || c == '-' || c == '*' ||
				c == '%' || c == '/' || c == '^')
		return (IS_SIGN);
	return (IS_UNHAND);
}

static int	oper_priority(char c)
{
	if (c == '^')
		return (10);
	else if (c == '*' || c == '/')
		return (8);
	else if (c == '+' || c == '-')
		return (6);
	return (0);
}

static int 	num_len(const char *expr)
{
	int 	i;

	i = 0;
	while (expr[i] && char_type(expr[i]) == IS_DIGIT)
	{
		++i;
	}
	return (i);
}

t_stack		*rpn_create(const char *expr)
{
	int 		i;
	t_stack		*stack;
	t_stack		*rpn;

	stack = t_stack_create(3, 0b10);
	rpn = t_stack_create(3, 0b10);
	if (!expr)
		return (NULL);
	i = 0;
	while (expr[i])
	{
		if (char_type(expr[i]) == IS_DIGIT)
		{
			t_stack_push(rpn, atoi(expr + i));
			rpn->info[rpn->elem_count - 1] = IS_DIGIT;
			i = i + num_len(expr + i) - 1;
		}
		else if (char_type(expr[i]) == IS_OPBR)
			t_stack_push(stack, '(');
		else if (char_type(expr[i]) == IS_CLBR)
		{
			while (!t_stack_isempty(stack))
			{
				if (t_stack_top(stack) != '(')
				{
					t_stack_push(rpn, t_stack_pop(stack));
					rpn->info[rpn->elem_count - 1] = IS_SIGN;
				}
				else
				{
					t_stack_pop(stack);
					break ;
				}
			}
		}
		else if (char_type(expr[i]) == IS_SIGN)
		{
			while (!t_stack_isempty(stack) &&
			oper_priority(expr[i]) <= oper_priority(t_stack_top(stack)))
			{
				t_stack_push(rpn, t_stack_pop(stack));
				rpn->info[rpn->elem_count - 1] = IS_SIGN;
			}
			t_stack_push(stack, expr[i]);
		}
		++i;
	}
	while (!t_stack_isempty(stack))
	{
		if (t_stack_top(stack) != '(')
			t_stack_push(rpn, t_stack_pop(stack));
		else
			t_stack_pop(stack);
		rpn->info[rpn->elem_count - 1] = IS_SIGN;
	}
	t_stack_print(rpn);
	return (rpn);
}