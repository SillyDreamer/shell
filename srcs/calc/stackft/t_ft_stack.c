#include "ft_stack.h"

t_stack			*t_stack_create(size_t init_sz, int type)
{
	t_stack *stack;

	stack = (t_stack *)malloc(sizeof(t_stack));
	if (stack)
	{
		stack->elem_count = 0;
		stack->elems = (long long *)malloc((init_sz > 0 ? init_sz : 5) * sizeof(long long));
		stack->max = (type & 0b1) ?
		(long long *)malloc((init_sz > 0 ? init_sz : 5) * sizeof(long long)) : NULL;
		stack->info = (type & 0b10) ?
		(int *)malloc((init_sz > 0 ? init_sz : 5) * sizeof(int)) : NULL;
		stack->mem_allocated = init_sz > 0 ? init_sz : 5;
	}
	return (stack);
}

void				t_stack_push(t_stack *stack, int value)
{
	if (stack)
	{
		if (stack->mem_allocated <= stack->elem_count)
		{
			stack->elems = realloc(stack->elems, stack->elem_count * 2 * sizeof(int));
			// stack->max = realloc(stack->max, stack->elem_count * 2);
			stack->info = realloc(stack->info, stack->elem_count * 2 * sizeof(int));
			stack->mem_allocated *= 2;
		}
		stack->elems[stack->elem_count] = value;
//		if (stack->max)
//		{
//			if (stack->elem_count == 0 ||
//			value > stack->max[stack->elem_count - 1])
//			{
//				stack->max[stack->elem_count] = value;
//			}
//			else
//				stack->max[stack->elem_count] =
//				stack->max[stack->elem_count - 1];
//		}
		++(stack->elem_count);
	}
}

int					t_stack_pop(t_stack *stack)
{
	if (stack && stack->elem_count > 0)
	{
		--stack->elem_count;
		return (stack->elems[stack->elem_count]);
	}
	return (0);
}

void				t_stack_destroy(t_stack *stack)
{
	if (stack)
	{
		free(stack->elems);
		free(stack->max);
		free(stack);
	}
}

int 				t_stack_isempty(t_stack *stack)
{
	if (!stack || stack->elem_count == 0)
		return (1);
	return (0);
}

int 				t_stack_top(t_stack *stack)
{
	if (stack && t_stack_isempty(stack))
		return (0);
	else
		return (stack->elems[stack->elem_count - 1]);
}

void				t_stack_print(t_stack *stack)
{
	size_t		i;

	i = 0;
	if (stack)
	{
		printf("Count: %zu Mem: %zu \n",
			stack->elem_count,
			stack->mem_allocated
			/* stack->max[stack->elem_count - 1] */);
		while (i < stack->elem_count)
		{
			printf("%lld  %d\n", stack->elems[i], stack->info ? stack->info[i] : 0);
			++i;
		}
		printf("\n");
	}
	else
		write(2, "NULL ptr\n", 9);
}