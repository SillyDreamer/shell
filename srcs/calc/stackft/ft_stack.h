/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lreznak- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/18 20:54:05 by lreznak-          #+#    #+#             */
/*   Updated: 2019/04/18 21:15:59 by lreznak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_STACK_H
# define FT_STACK_H
# include <stdlib.h>
# include <math.h>
# include <stdio.h>
# include <unistd.h>

typedef struct s_stack t_stack;

struct s_stack
{
	long long			*elems;
	long long			*max;
	int 				*info;
	size_t		 		elem_count;
	size_t				mem_allocated;
};

/*
**	stack type:
**
**  Here are the reversed representation of int bits:
**	_______________________________
** 	| max |  info  |    |    |    |
**	_______________________________
**
**  So 0b1 type is stack with max support
**
**  max: initializes max int* stack and supports stack max.
**  info: allocates info int* for custom user data
**
*/

t_stack				*t_stack_create(size_t init_sz, int type);
void				t_stack_push(t_stack *stack, int value);
int					t_stack_pop(t_stack *stack);
void				t_stack_print(t_stack *stack);
void				t_stack_destroy(t_stack *stack);
int 				t_stack_isempty(t_stack *stack);
int 				t_stack_top(t_stack *stack);


#endif
