#include "rpn_handler.h"

static long long 	reverse_mod(long long m, long long n)
{
	return (n % m);
}

static long long	reverse_pow(long long n, long long a)
{
	int res;

	res = 1;
	while (n)
		if (n & 1)
		{
			res *= a;
			--n;
		}
		else
		{
			a *= a;
			n >>= 1;
		}
	return (res);
}

long long 			rpn_handler(t_stack *rpn)
{
	t_stack			*stack;
	int 			sign;
	int 			i;

	stack = t_stack_create(3, 0b0);
	i = -1;
	while (++i < rpn->elem_count)
	{
		if (rpn->info[i] == RPN_DECIMAL)
			t_stack_push(stack, rpn->elems[i]);
		else if (rpn->info[i] == RPN_SIGN)
		{
			sign = rpn->elems[i];
			if (sign == '+')
				t_stack_push(stack,
						t_stack_pop(stack) + t_stack_pop(stack));
			else if (sign == '-')
				t_stack_push(stack,
						-t_stack_pop(stack) + t_stack_pop(stack));
			else if (sign == '*')
				t_stack_push(stack,
						t_stack_pop(stack) * t_stack_pop(stack));
			else if (sign == '/')
				t_stack_push(stack, (int)(1.0 / (float)t_stack_pop(stack) 				*(float)t_stack_pop(stack)));
			else if (sign == '%')
				t_stack_push(stack,
						reverse_mod(t_stack_pop(stack), t_stack_pop(stack)));
			else if (sign == '^')
				t_stack_push(stack,
						reverse_pow(t_stack_pop(stack), t_stack_pop(stack)));
		}
	}
	t_stack_print(stack);
	return (t_stack_pop(stack));
}
