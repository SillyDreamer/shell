#include "rpn_handler.h"

int 		main(void)
{
	t_stack *rpn = t_stack_create(10, 0b10);
	t_stack_push(rpn, 10);
	rpn->info[0] = RPN_DECIMAL;
	t_stack_push(rpn, 2);
	rpn->info[1] = RPN_DECIMAL;
	t_stack_push(rpn, '/');
	rpn->info[2] = RPN_SIGN;
	t_stack_print(rpn);
	printf("%d\n", rpn_handler(rpn));
	t_stack_destroy(rpn);
}