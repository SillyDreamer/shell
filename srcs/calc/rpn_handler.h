#ifndef RPN_HANDLER_H
#define RPN_HANDLER_H

# define RPN_DECIMAL	1
# define RPN_SIGN		2

# include "stackft/ft_stack.h"

long long 			rpn_handler(t_stack *rpn);

#endif
