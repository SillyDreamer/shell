/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_path.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 03:15:14 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:26:45 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

char		*parse_path(char *args, int i)
{
	char		**path;
	DIR			*fd;
	t_dirent	*ret;
	char		*bin_path;
	char		*s;

	if (!(s = getenv("PATH")))
		return (NULL);
	path = ft_strsplit(s, ':');
	while ((path[++i]))
	{
		fd = opendir(path[i]);
		while ((ret = readdir(fd)))
			if (ft_strequ(ret->d_name, args))
			{
				bin_path = ft_strjoin2(path[i], "/", args);
				closedir(fd);
				free_mas(path);
				return (!access(bin_path, 1) ? bin_path : 0);
			}
		fd ? closedir(fd) : 0;
	}
	path[0] ? free_mas(path) : 0;
	return (NULL);
}

int			check_path(char *args, int i)
{
	char		**path;
	DIR			*fd;
	t_dirent	*ret;
	char		*s;

	if (!(s = getenv("PATH")))
		return (0);
	path = ft_strsplit(s, ':');
	while ((path[++i]))
	{
		fd = opendir(path[i]);
		while ((ret = readdir(fd)))
			if (access(ret->d_name, 1) &&
			ft_strncmp(ret->d_name, args, ft_strlen(args)) == 0)
			{
				free_mas(path);
				fd ? closedir(fd) : 0;
				return (1);
			}
		fd ? closedir(fd) : 0;
	}
	free_mas(path);
	return (0);
}
