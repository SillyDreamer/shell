/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   shapka                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 20:32:53 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 20:32:53 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MAIN_H
# define MAIN_H

# include "../includes/minishell.h"





/*
**ft_path.c
*/
char	*parse_path(char *args, int i);
int		check_path(char *args, int i);
/*
**ft_signal.c
*/
void	ft_signal(int signo);
void	ft_signal2(int signo);
/*
**ft_tty.c
*/
void	tty(int status);
/*
**main.c
*/
int		msh_execute(char **args);
char	*read_line(t_history *his);
/*
**utils.c
*/
void	free_mas(char **mas);
#endif
