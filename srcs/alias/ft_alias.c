/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_alias.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 21:37:18 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 21:09:43 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/minishell.h"

t_vec		*ft_alias(void)
{
	char		*line;
	int			fd;

	g_vec = vec_init(2);
	fd = open("./srcs/alias/alias", O_RDONLY);
	while (get_next_line(fd, &line) > 0)
	{
		vec_push(g_vec, line);
		free(line);
	}
	free(line);
	return (g_vec);
}

void		alias_print(void)
{
	int		l;

	l = 0;
	while (l < g_vec->total)
	{
		if (((char **)g_vec->data)[l][0])
			ft_printf("%s\n", ((void **)g_vec->data)[l]);
		l++;
	}
}

int		alias(char **mas)
{
	if (!mas[1])
		alias_print();
	else
		vec_push(g_vec, mas[1]);
	return (1);
}

int		unalias(char **mas)
{
	int		j;
	int		i;
	char	tmp[1024];

	i = -1;
	if (!mas[1])
		return (0);
	while (++i < g_vec->total)
	{
		ft_bzero(tmp, 1024);
		j = -1;
		while (((char **)g_vec->data)[i][++j] &&
		((char **)g_vec->data)[i][j] != '=')
			tmp[j] = ((char **)g_vec->data)[i][j];
		if (ft_strcmp(mas[1], tmp) == 0)
		{
			ft_bzero(((char **)g_vec->data)[i],
			ft_strlen(((char **)g_vec->data)[i]));
			break ;
		}
	}
	return (1);
}
