/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tty.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/25 00:04:14 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/27 16:19:54 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/minishell.h"

void	tty(int status)
{
	static struct termios	stored_settings;
	static struct termios	new_settings;
	static int				used;

	used = 0;
	if (!used)
	{
		used++;
		tcgetattr(0, &stored_settings);
	}
	new_settings = stored_settings;
	new_settings.c_lflag &= (~ICANON & ~ECHO);
	new_settings.c_cc[VTIME] = 0;
	new_settings.c_cc[VMIN] = 1;
	if (status)
		tcsetattr(0, TCSANOW, &new_settings);
	else if (!status)
		tcsetattr(0, TCSANOW, &stored_settings);
}
