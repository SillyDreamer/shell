NAME = minishell

CFLAGS = -I ./libft/includes/ -I srcs/ft_printf/ -I ./includes

SRC =	./getnextline/get_next_line.c \
		./srcs/main.c ./srcs/ft_tty.c ./srcs/utils.c ./srcs/ft_path.c ./srcs/ft_signal.c \
		./srcs/builtins/ft_help.c ./srcs/builtins/ft_unsetenv.c ./srcs/builtins/ft_comands.c \
		./srcs/builtins/ft_echo.c ./srcs/builtins/ft_eenv.c ./srcs/builtins/ft_setenv.c ./srcs/builtins/ft_cd.c \
		./srcs/readline/ft_autocomp_list.c ./srcs/readline/ft_autocomp_print_list.c ./srcs/readline/ft_autocomp.c \
		./srcs/readline/ft_autocomp_print.c ./srcs/readline/ft_autocomp_termcap.c ./srcs/readline/ft_autocomp_utils.c \
		./srcs/readline/ft_brackets_quotes.c ./srcs/readline/ft_edit_line.c ./srcs/readline/ft_globbing_list.c \
		./srcs/readline/ft_globbing.c ./srcs/readline/ft_history.c ./srcs/readline/ft_readline_var.c \
		./srcs/readline/ft_keys_move.c ./srcs/readline/ft_keys.c ./srcs/alias/ft_alias.c \
		./srcs/readline/ft_alias_var.c \


OBJ = $(SRC:.c=.o)

LIBFT = libft/libft.a

all: $(NAME)

$(OBJ): %.o: %.c
	@gcc -g -c $(CFLAGS) $< -o $@ 

$(LIBFT):
	@make -C ./libft

$(NAME): $(LIBFT) $(OBJ)
	@gcc $(OBJ) $(LIBFT) -o $(NAME) -ltermcap
	@echo "\x1B[34m(✿｡✿)"

clean:
	@rm -rf $(OBJ)
	@make -C ./libft clean

fclean: clean
	@rm -rf $(NAME)
	@make -C ./libft fclean

re: fclean all
