/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 19:48:09 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 23:37:45 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include <unistd.h>
# include <stdio.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/wait.h>
# include <sys/stat.h>
# include <dirent.h>
# include <sys/ioctl.h>
# include <sys/dir.h>
# include <termios.h>
# include <string.h>
# include <termcap.h>
# include "../libft/includes/libft.h"

# define NOTEXIST "cd: no such file or directory: %s\n"
# define NOTDIR "cd: not a directory: %s\n"
# define NOPERM "cd: permission denied: %s\n"
# define CD_ERNOTEXIST(p, e) ({ft_printf(NOTEXIST, p); return (e);})
# define CD_ERNOTDIR(p, e) ({ft_printf(NOTDIR, p); return (e);})
# define CD_ERPERM(p, e) ({ft_printf(NOPERM, p); return (e);})
# define PRINT(str, e) ({ft_printf(str); return (e);})

# define NRM "\x1B[0m"
# define RED "\x1B[31m"
# define GRN "\x1B[32m"
# define YEL "\x1B[33m"
# define BLU "\x1B[34m"
# define MAG "\x1B[35m"
# define CYN "\x1B[36m"

# define LEFT 4479771
# define RIGHT 4414235
# define TAB 9
# define ENTER 10
# define BACKSPASE 127
# define UP 4283163
# define DOWN 4348699
# define CTRL_RIGHT	11 //move right args
# define CTRL_LEFT 21 //move left args
# define CTRL_D 4
# define LEFT_LEFT 4741915 //move left string
# define RIGHT_RIGHT 4610843 //move right string
# define CTRL_A 1 // select
# define CTRL_W 23 // cut
# define CTRL_E 5 // copy
# define CTRL_T 20	 // paste
# define CTRL_R 18 // search
# define CTRL_UP 79393463556891 // up
# define CTRL_DOWN 77194440301339 // down

# define QUOTES ft_putstr("\nquote>  ");
# define BQUOTES ft_putstr("\nbquote>  ");
# define DQUOTES ft_putstr("\ndquote>  ");

char **g_eenv;
char g_copy[1024];

typedef struct dirent		t_dirent;
typedef struct winsize		t_win;

typedef enum				e_quo
{
	off,
	quotes,
	dquotes,
	bquotes
}							t_quo;


typedef struct 				s_history
{
	struct s_history		*next;
	struct s_history		*prev;
	char					*line;
}							t_history;

typedef struct				s_glob
{
	struct s_glob			*next;
	char					*name;
	int						len;
}							t_glob;

typedef struct 				s_all
{
	int						i;
	int						j;
	int						y;
	int						y2;
	int						h;
	int						flag;
	t_quo					quotes;
	t_vec					*vec;
}							t_all;

t_all	*g_all;
t_vec	*g_vec;

typedef struct 				s_tab
{
	struct s_tab			*next;
	struct s_tab			*prev;
	char					*name;
	int						select;
	int						begin;
	int						len;
	int						type;
	int						col;
}							t_tab;

/*
** builtins **
*/


/*
** in ft_comands.c
*/
int				check_commands(char **s);
int				msh_comands(char **commands);
int				msh_exit(void);
int				msh_pwd(char **args);

/*
** in ft_eenv.c
*/
int				check_eenv(char *name);
int				print_eenv(void);
int				strlen_geenv(void);
void			init_eenv(char **eenv);

/*
** in ft_setenv.c
*/
char			**realloc_eenv(int new_size);
void			change_eenv(int i, char *name, char *value);
void			add_eenv(char *name, char *value);
int				set_env(char **args);

/*
** in ft_unsetenv.c
*/
int				unset_envv(char **args);

/*
** in ft_cd.c
*/
void			change_pwd(char *oldpwd);
int				msh_cd(char **args);

/*
** in ft_eho.c
*/
int				msh_echo(char **args);

/*
** in ft_help.c
*/
int				help(void);




/*
**
** readline
**
*/

/*
** in ft_keys.c
*/
int				check_key(long key, char *string, int *index);

/*
** in ft_brackets_quotes.c
*/
void			check_quotes(long key, int *index_quotes);

/*
** in ft_edit_line.c
*/
void			key_backspace(char *string);
void			not_keys(long key, char *string);
void			key_shift_e(char *string);
void			key_shift_r(char *string);

/*
** in ft_history.c
*/
t_history		*init_list(char *line);
t_history		*make_list(t_history **list, t_history *elem);
t_history		*key_down(t_history *his, char *string);
t_history		*key_up(t_history *his, char *string);

/*
** in ft_keys_move.c
*/
void			key_right_left(long key);
void			key_ctrl_left(long key, char *string);
void			left_right(long key);

/*
** in ft_readline_var.c
*/
void			check_var(char *string);

/*
** in ft_autocomp.c
*/
void			complet_command(char *string, int i, int flag);
void			complet_args(char *string);
void			complet_dollar(char *string);
void			complet_cd(char *string);
void			complet_arguments(char *string);

/*
** in ft_autocomp_list.c
*/
void			free_list(t_tab *begin);
t_tab			*init__first_list(char *name, int type);
void			make__list(t_tab **tab, char *name, int type);
void			next_begin(t_tab **begin, int flag);

/*
** in ft_autocomp_termcap.c
*/
void			move(int size);
void			key_tab_enter2(t_tab *begin, char *string);
void			key_tab_enter(t_tab *begin, char *string);
int				key_tab_tab(int key, t_tab *begin, int size);
int				key_tab_tab2(int key, t_tab *begin, int size, char *string);

/*
** in ft_autocomp_print.c
*/
void			print2(t_tab *begin, int flag, char *string, int flag2);
void			print(t_tab *begin, int flag, char *string);
void			print_type(int type, char *name, int flag, int max);

/*
** in ft_autocomp_print_list.c
*/
void			print_list(t_tab *tab, int flag);

t_tab			*utils(t_tab *begin, int flag, char *name, int type);

/*
** in ft_globbing_list.c
*/
void			free_glob_list(t_glob *begin);
t_glob			*create_glob(char *name);
t_glob			*init_glob(char *name, t_glob *glob);

/*
** in ft_globbing.c
*/
void			globing(char *string);


/*
**
** main
**
*/

/* 
** in ft_signal.c
*/
void			ft_signal2(int signo);
void			ft_signal(int signo);

/*
** in ft_tty.c
*/
void			tty(int status);

/*
** in utils.c
*/
void			free_mas(char **mas);

/*
** in ft_path.c 
*/
char			*parse_path(char *args, int i);
int				check_path(char *args, int i);

/*
** in main.c
*/
int				msh_execute(char **args);

t_vec		*ft_alias(void);
void		alias_print(void);
int			alias(char **mas);
void				check_alias(char *string);
int		unalias(char **mas);
void	free_vec(void);

#endif