/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp_registr.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/21 21:27:31 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/21 22:19:02 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/str_ft.h"

int		ft_strncmp_registr(char *file, char *s, int n)
{
	int i;

	i = 0;
	if (n == 0)
		return (0);
	while (n && file[i] && s[i])
	{
		if (s[i] >= 65 && s[i] <= 90)
			if (s[i] == file[i] || s[i] + 32 == file[i])
				i++;
			else
				return (s[i] - file[i]);
		else if (s[i] >= 97 && s[i] <= 122)
			if (s[i] == file[i] || s[i] - 32 == file[i])
				i++;
			else
				return (s[i] - file[i]);
		else if (s[i] == file[i])
			i++;
		else
			return (s[i] - file[i]);
		n--;
	}
	return (0);
}
