/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 07:46:09 by rgalyeon          #+#    #+#             */
/*   Updated: 2019/04/19 02:28:50 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/str_ft.h"

char	*ft_strrchr(const char *s, int chr)
{
	char *ptr;
	char c;

	ptr = NULL;
	while (*s)
	{
		c = *s;
		if (c == (char)chr)
			ptr = (char *)s;
		s++;
	}
	if (chr == 0)
		return ((char *)s);
	return (ptr + 1);
}
