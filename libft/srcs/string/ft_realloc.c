/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/04 02:16:33 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/03/04 08:51:33 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../includes/libft.h"

char	*ft_realloc(char *i, int size)
{
	char	*j;
	int		k;

	k = 0;
	j = (char*)ft_memalloc(sizeof(char) * (size + 1));
	while (k < size && i[k])
	{
		j[k] = i[k];
		k++;
	}
	free(i);
	return (j);
}
