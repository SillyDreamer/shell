/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_push.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 20:41:51 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 17:55:39 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../../../includes/vec_ft.h"

t_vec	*vec_push(t_vec *vec, void *content)
{
	void	*tmp;

	if (!vec)
		return (NULL);
	tmp = vec->data;
	if (vec->total >= vec->capacity)
	{
		if (!(vec->data = malloc(sizeof(void *) * (vec->capacity * 2))))
		{
			free(tmp);
			free(vec);
			return (NULL);
		}
		vec->capacity *= 2;
		ft_memcpy(vec->data, tmp, sizeof(void *) * vec->total);
		free(tmp);
		(((void **)vec->data))[vec->total] = ft_strcpy(ft_strnew(ft_strlen((char*)content)), content);
		vec->total += 1;
	}
	else
	{
		(((void **)vec->data))[vec->total] = ft_strcpy(ft_strnew(ft_strlen((char*)content)), content);
		vec->total += 1;
	}
	return (vec);
}
