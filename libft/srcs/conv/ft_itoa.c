/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 21:44:57 by ghazrak-          #+#    #+#             */
/*   Updated: 2018/11/28 00:38:14 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		len(int n)
{
	int i;

	i = 1;
	while ((n /= 10) > 0)
		i++;
	return (i);
}

char			*ft_itoa(int n)
{
	int		i;
	int		sign;
	char	*a;
	int		n2;

	sign = 0;
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	if (n < 0)
		sign = 1;
	if (n < 0)
		n = -n;
	n2 = n;
	i = len(n);
	if (!(a = (char*)malloc(sizeof(char) * i + 1 + sign)))
		return (0);
	i = 0;
	a[i++] = n2 % 10 + '0';
	while ((n2 /= 10) > 0)
		a[i++] = n2 % 10 + '0';
	if (sign == 1)
		a[i++] = '-';
	a[i] = '\0';
	ft_strrev(a);
	return (a);
}
