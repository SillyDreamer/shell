/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   vec_ft.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/27 20:40:45 by ghazrak-          #+#    #+#             */
/*   Updated: 2019/04/28 00:03:46 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef VEC_FT_H
# define VEC_FT_H

# include "libft.h"

typedef struct	s_vec
{
	void	*data;
	size_t	capacity;
	size_t	total;
}				t_vec;

t_vec	*vec_init(size_t capacity);
t_vec	*vec_push(t_vec *vec, void *content);

#endif