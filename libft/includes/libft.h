/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ghazrak- <ghazrak-@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/23 16:32:31 by rgalyeon          #+#    #+#             */
/*   Updated: 2019/04/27 20:43:28 by ghazrak-         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <string.h>
# include <unistd.h>

# include "char_ft.h"
# include "conv_ft.h"
# include "lst_ft.h"
# include "memory_ft.h"
# include "put_ft.h"
# include "str_ft.h"
# include "sort_ft.h"
# include "get_next_line.h"
# include "ft_printf.h"
# include "vec_ft.h"

#endif
