cmake_minimum_required(VERSION 3.13)
project(21sh C)

set(CMAKE_C_STANDARD 11)

FILE(GLOB FT_LS_SRC ./srcs/*.c)
include_directories(./inc/)

#add_library(myLogicalExtLib SHARED IMPORTED)



#set_target_properties(myLogicalExtLib PROPERTIES IMPORTED_LOCATION "/Users/lreznak-/Documents/ls/libft.a")


add_executable(21sh ${FT_LS_SRC})
target_link_libraries(21sh "/Users/lreznak-/Documents/ls/libft.a")